
class Dojo {

    /**
     * 
     * Initializer that can be used to set global state, do precomputation, etc.
     * 
     * @param canvas HTML5 canvas DOM element
     * @param ctx the 2d canvas context
     * @param {int} width canvas width in pixel
     * @param {int} height canvas height in pixel
     */
    constructor({canvas, ctx, width, height}) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.width = width;
        this.height = height;

        new FontFace(
            "Poppins",
            "url(https://fonts.gstatic.com/s/poppins/v20/pxiEyp8kv8JHgFVrJJfecnFHGPc.woff2)",
        ).load().then((font) => document.fonts.add(font));
    }

    /**
     * 
     * Callback called when canvas is clicked
     * 
     * @param {x, y} mouse current mouse position
     */
    onMouseClicked({mouse}) {}

    /**
     * 
     * Callback called when mouse is moved over canvas
     * 
     * @param {x, y} mouse current mouse position
     */
    onMouseMoved({mouse}) {}

    /**
     * 
     * Function call to draw a frame
     * 
     * @param ctx the 2d canvas context
     * @param {float} time elapsed time in seconds since first frame
     */
    draw({ctx, time}) {
        ctx.fillStyle = '#fff';
        ctx.fillRect(0, 0, this.width, this.height);

        const fontSize = 60 + (Math.sin(time) * 0.5 + 0.5) * 12;
        ctx.font = 'bold ' + fontSize + 'px Poppins';

        const text = 'BeTomorrow';
        const textOffsetX = ctx.measureText(text).width / 2;

        ctx.save();
        {
            ctx.translate(300, 315);
            ctx.save();
            {
                ctx.translate(3, 0);
                ctx.scale(1, 0.3);
                const gradient = ctx.createLinearGradient(0, 0, 0, -60);
                gradient.addColorStop(0.2, 'rgba(0, 0, 0, 0.4)');
                gradient.addColorStop(0.7, 'rgba(0, 0, 0, 0.2)');
                ctx.fillStyle = gradient;
                ctx.font = 'italic bold ' + fontSize + 'px Poppins';
                ctx.fillText(text, -textOffsetX, 0);
            }
            ctx.restore();

            ctx.fillStyle = '#232077';
            ctx.fillText(text, -textOffsetX, 0);

            ctx.translate(-1, 1);
            ctx.fillStyle = '#2720f1';
            ctx.fillText(text, -textOffsetX, 0);

            ctx.translate(-1, 1);
            ctx.fillStyle = '#4242ff';
            ctx.fillText(text, -textOffsetX, 0);
        }
        ctx.restore();    
    }
}

# Dojo creative developper

https://gitlab.com/dsarrio/dojo-creadev-canvas

## Setup

Pour lancer le viewer en mode auto-reload

```
> npm install
> npm run start
```

## Exercice

Chercher à reproduire l'effet présenté en début de session. Il n'est pas nécessaire de chercher le pixel perfect, au contraire, laissez votre créativité aller au delà de ce qui est demandé !
